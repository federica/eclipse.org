server {
  listen 8080;
  server_name localhost;
  port_in_redirect off;

  # access_log  /var/log/nginx/host.access.log  main;

  # client_max_body_size 16m;
  # client_body_buffer_size 128k;

  # proxy buffering configuration
  # https://stackoverflow.com/a/27551259/8538422
  # proxy_buffer_size 128k;
  # proxy_buffers 4 256k;
  # proxy_busy_buffers_size 256k;

  # location /css {
  #   alias /usr/share/nginx/html/css;
  # }

  # location /fonts {
  #   alias /usr/share/nginx/html/fonts;
  # }

  # location /home {
  #   alias  /usr/share/nginx/html/home;
  # }

  # location /images {
  #   alias /usr/share/nginx/html/images;
  # }

  # location /js {
  #   alias /usr/share/nginx/html/js;
  # }

  # location /sitemap.xml {
  #   alias  /usr/share/nginx/html/sitemap.xml;
  # }

  # location /robots.txt {
  #   alias  /usr/share/nginx/html/robots.txt;
  # }

  error_page 404 /404.html;

  location / {
    # www.eclipse.org/home
    # https://git.eclipse.org/c/www.eclipse.org/home.git/tree/
    rewrite /home/search.php /home/search redirect;
    rewrite /home/newcomers.php /home/newcomers redirect;
    rewrite /home/welcome.php /home/welcome redirect;
    rewrite /home/whatis.php /home/whatis redirect;

    # www.eclipse.org/getting_started
    # https://git.eclipse.org/c/www.eclipse.org/getting_started.git/tree/
    rewrite /getting_started /getting-started redirect;
    rewrite /getting_started/index.php /getting-started redirect;

    # www.eclipse.org/documentation
    # https://git.eclipse.org/c/www.eclipse.org/documentation.git/tree/
    rewrite /documentation/index.php /documentation redirect;

    # www.eclipse.org/europe
    # https://git.eclipse.org/c/www.eclipse.org/europe.git/tree/
    rewrite /europe/index.php /europe redirect;
    rewrite /europe/faq.php /europe/faq redirect;
    rewrite /europe/aisbl_membership_faq.php /europe/aisbl-membership-faq redirect;

    # www.eclipse.org/ide
    # https://git.eclipse.org/c/www.eclipse.org/ide.git/tree/
    rewrite /ide/index.php /ide redirect;

    # www.eclipse.org/contribute
    # https://git.eclipse.org/c/www.eclipse.org/contribute.git/tree/
    rewrite /contribute/dev_program /contribute/dev-program redirect;
    rewrite /contribute/dev_program.php /contribute/dev-program redirect;
    rewrite /contribute/dev_program/index.php /contribute/dev-program redirect;
    rewrite /contribute/dev_program/faq.php /contribute/dev-program/faq redirect;
    rewrite /contribute/cla https://accounts.eclipse.org/user/eca redirect;
    rewrite /contribute/cla/index.php https://accounts.eclipse.org/user/eca redirect;
    
    # www.eclipseide.org/getting-started
    # www.eclipse.org/getting-started
    rewrite /getting-started https://eclipseide.org/getting-started redirect;

    # www.eclipse.org/openchain
    rewrite /projects/openchain /openchain redirect;

    # www.eclipse.org/security
    # https://git.eclipse.org/c/www.eclipse.org/security.git/tree/
    rewrite /security/index.php /security/ redirect;
    rewrite /security/known.php /security/known/ redirect;
    rewrite /security/policy.php /security/policy/ redirect;
    rewrite /security/policy_2011.php /security/policy/ redirect;

    root /usr/share/nginx/html/;
    index index.html index.htm;
  }

  # redirect server error pages to the static page /50x.html
  error_page 500 502 503 504 /50x.html;
  location = /50x.html {
    root /usr/share/nginx/html;
  }

  # deny access to .htaccess files, if Apache's document root
  # concurs with nginx's one
}
