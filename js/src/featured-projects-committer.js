(function($, document) {

  $.ajax({
      url: "https://newsroom.eclipse.org/api/edition?options[orderby][nid]=DESC&pagesize=1",
      type: 'GET',
      dataType: 'json',
      success: function(data, status) {

          if (data.editions[0].featured_committer !== undefined && data.editions[0].featured_committer.length !== 0) {

              $(".featured-committer-project-section").css('display', 'block');
              $(".featured-committer-section").css('display', 'block');

              const committer = data.editions[0].featured_committer
              var image = "<div class='col-xs-8 padding-0 eclipsefdn-user-display-circle'><img class='img img-responsive' src=" + committer.picture + "></div>"
              var content = " <div class='col-xs-16'>" +
              "<h3 class='h4'><strong>" + committer.name + "</strong></h3>" +
              "<div class=\"featured-blocks-match-height margin-bottom-5\" data-mh=\"featured-content\"><p>" + committer.body + "</p>" +
              "</div>" +
              "<a class='btn btn-primary' href='" + committer.url + "' title='Read more about "+ committer.name +"'> Read more </a>" +
              "</div>"

              var result = "<div class='item clearfix block-summary-item featured-blocks-match-height-by-row'>" + image + content + "</div>"

              $('#commiters-container').append(result);
          }
          if (data.editions[0].featured_project !== undefined && data.editions[0].featured_project.length !== 0) {

              $(".featured-committer-project-section").css('display', 'block');
              $(".featured-project-section").css('display', 'block');

              const project = data.editions[0].featured_project;
              var project_id = project.id;
              var project_id = project_id.replace(/\./g, "_");

              $.ajax({
                  url: "https://projects.eclipse.org/api/projects/" + project_id,
                  type: 'GET',
                  dataType: 'json',
                  success: function (data, status) {

                      var image = "<div class='col-xs-8 padding-0'><img class='img img-responsive' src=" + data[0].logo + "></div>"
                      var content = " <div class='col-xs-16'>" +
                      "<h3 class='h4'><strong>" + data[0].name + "</strong></h3>" +
                      "<div class=\"featured-blocks-match-height margin-bottom-5\" data-mh=\"featured-content\"><p>" + project.body + "</p></div>" +
                      "<a class='btn btn-primary' href='" +  data[0].url + "' title='Read more about " + data[0].name + "'> Read more </a>" +
                      "</div>"

                      var result = "<div class='item clearfix block-summary-item featured-blocks-match-height-by-row'>" + image + content + "</div>"

                      $('#projects-container').append(result);
                  }
              });
          }
          $('.featured-blocks-match-height-by-row').matchHeight();
          $('.featured-blocks-match-height').matchHeight({
              byRow: false
          });
      },
      error: function() {
          console.log("An error has occurred.");
      }
  });


})(jQuery, document);



