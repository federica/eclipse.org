---
title: "Research @ Eclipse Foundation"
headline: "Research @ Eclipse Foundation"
cascade:
  hide_call_for_action : true
  description: Business-friendly open source and open innovation underpins exploitation, community building and dissemination strategy for European projects.
  keywords: ["eclipse", "research"]
  seo_title_suffix : " | Research | The Eclipse Foundation"
  page_css_file: public/css/research-styles.css

hide_sidebar: true
show_featured_story: false
date: 2019-09-10T15:50:25-04:00
hide_page_title: true
#hide_breadcrumbs: true
show_featured_story: false
show_featured_footer: false

links: [[href: "#projects", text: "Research Projects"], [href: "#collaborations", text: "Collaborations"]]
container: "container-fluid"
header_wrapper_class: "header-default-bg-img small-jumbotron-subtitle"
page_css_file : public/css/research-styles.css
jumbotron_class: "col-sm-24"
custom_jumbotron_class: "col-sm-24"
custom_jumbotron: |
    <div class="home-custom-jumbotron row">
        <p class="col-sm-14">
            Business-friendly open source and open innovation underpin exploitation, community building, and dissemination strategies for European projects
        </p>
        <div class="research-logo-wrapper col-sm-10">
            <img src="images/eclipse-research-logo-transparent.svg" class="research-logo" alt="Eclipse Research Logo" />
        </div>
    </div>
---

{{< pages/research/home-section-header >}}

{{< pages/research/home-section-info >}}

{{< pages/research/home-section-opportunities >}}

{{< pages/research/home-section-organizations >}}

{{< grid/section-container id="projects" class="row-gray padding-top-40 padding-bottom-40" isMarkdown="false" >}}
    <h2 class="text-center margin-bottom-40">The Eclipse Foundation is a Partner in these Projects</h2>
    {{< eclipsefdn_projects is_static_source="true" url="/research/projects/index.json" templateId="tpl-projects-item-research" display_view_more="false" >}}
{{</ grid/section-container >}}

{{< pages/research/collaboration-section >}}

{{< pages/research/newsroom-section id="collaborations" working_group="research" class="margin-bottom-30" resource_class="col-md-5 newsroom-resource-card newsroom-resource-card-match-height match-height-item" >}}

{{< pages/research/home-section-contact >}}

{{< mustache_js template-id="tpl-projects-item-research" path="/js/src/templates/research/tpl-projects-item-research.mustache" >}}
