---
title: "SPADE"
date: 2022-09-01T00:00:00-00:00
project_logo: "/research/images/research/r-projects/spade.png"
tags: ["Agriculture", "Forestry", "Rural Development", "Geo-information", "spatial data analysis", "Imaging", "image processing"]
homepage: ""
facebook: ""
linkedin: ""
twitter: ""
youtube: ""
funding_bodies: ["horizon2020"]
eclipse_projects: []
project_topic: "Agriculture"
summary: "multi-purpoSe Physical-cyber Agri-forest Drones Ecosystem for governance and environmental observation"
hide_page_title: true
hide_sidebar: true
container: "bg-white"
header_wrapper_class: "header-projects-bg-img"
description: "# **SPADE**

Τhe strategic objective of SPADE is to develop an Intelligent Ecosystem to address the multiple purposes concept in the light of deploying UAVs to promote sustainable digital services for the benefit of a large scope of various end users in the sectors of agriculture, forestry, and livestock. This includes individual UAV usability, UAV type applicability (e.g., swarm, collaborative, autonomous, tethered), UAV governance models availability and trustworthiness. Multi-purposes will be further determined in the sensing dataspace reusability based on trained AI/Machine Learning (ML) models. These will enable sustainability and resilience of the overall life cycle of developing, setting up, offering, providing, testing, validating, refining as well as enhancing digital transformations and ‘innovation building’ services in Forestry, Cropping and Livestock Farming. Pilot prototypes will contribute towards greater challenges such as deforestation, precision cropping and animal welfare.


First, SPADE will create a digital platform that is able to realise the potential benefits to be reaped from the use of drones. This platform is making drone operations better accessible and controllable, as well as providing a service channel for value added services enabled by drones. Second, SPADE is demonstrating three innovative use cases of drones making use of the digital platform. While demonstrating the use cases, the benefits coming from the use of drones are analysed and quantified, on a detailed stakeholder level basis. This will demonstrate the new business opportunities. The demonstrations/pilots will also serve as an analysis platform to investigate the regulatory framework at international and national level. Open calls will provide 12 further use cases.


This project is running from September 2022 - August 2026."
---

{{< grid/div class="bg-light-gray" isMarkdown="false">}}
{{< grid/div class="container research-page-section" >}}

# Consortium
* Centre For Research and Technology Hellas - Greece
* Hafenstrom - Norway
* Nydor System Technologies Ae - Greece
* University Of Southern Denmark - Denmark
* Fraunhofer-Gesellschaft zur Foerderung Der Angewandten Forschung E.V - Germany
* Gradiant - Spain
* Sociedade Portuguesa De Inovação - Consultadoria Empresarial E Fomento Da Inovação S.A - Portugal
* University Of OULU - Finland
* Norwegian Institute of Bioeconomy Research - Norway
* Norwegian University of Science and Technology - Norway
* Aristotle University of Thessaloniki - Greece
* Trialog - France
* Eclipse Foundation Europe Gmbh - Germany
* Splorotech S.L.U. - Spain
* Bavenir Sro - Slovakia
* farmB Digital Agriculture - Greece
* Aarhus University - Denmark
* University Of Lincoln - UK
* Anysolution SL - Spain
* Mallorcan New Potatoes - Spain
* Cooperativa De Sóller - Spain

{{</ grid/div>}}
{{</ grid/div>}}
