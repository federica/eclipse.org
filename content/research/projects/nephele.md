---
title: "NEPHELE"
date: 2022-09-01T00:00:00-00:00
project_logo: "/research/images/research/r-projects/nephele-logo.png"
tags: ["IoT", "Software engineering", "Operating Systems", "computer languages", "Networks", "Open Source"]
homepage: ""
facebook: ""
linkedin: ""
twitter: ""
youtube: ""
funding_bodies: ["horizon2020"]
eclipse_projects: []
project_topic: "Cloud-Edge"
summary: "A lightweight software stack and synergetic meta-orchestration framework for the next generation compute continuum"
hide_page_title: true
hide_sidebar: true
container: "bg-white"
header_wrapper_class: "header-projects-bg-img"
description: "# **NEPHELE**

The vision of NEPHELE is to enable the efficient, reliable and secure end-to-end orchestration of hyper-distributed applications over programmable infrastructure that is spanning across the compute continuum from Cloud-to-Edge-to-IoT, removing existing openness and interoperability barriers in the convergence of IoT technologies against cloud and edge computing orchestration platforms, and introducing automation and decentralized intelligence mechanisms powered by 5G and distributed AI technologies. The NEPHELE project aims to introduce two core innovations, namely:

* (i) an IoT and edge computing software stack for leveraging virtualization of IoT devices at the edge part of the infrastructure and supporting openness and interoperability aspects in a device-independent way. Through this software stack, management of a wide range of IoT devices and platforms can be realised in a unified way, avoiding the usage of middleware platforms, while edge computing functionalities can be offered on demand to efficiently support IoT applications’ operations.

*(ii) a synergetic meta-orchestration framework for managing the coordination between cloud and edge computing orchestration platforms, through high-level scheduling supervision and definition, based on the adoption of a “system of systems” approach.


The NEPHELE outcomes are going to be demonstrated, validated and evaluated in a set of use cases across various vertical industries, including areas such as disaster management, logistic operations in ports, energy management in smart buildings and remote healthcare services. Two successive open calls will also take place, while a wide open-source community is envisaged to be created for supporting the NEPHELE outcomes.


This project is running from January 2021 - December 2023."
---

{{< grid/div class="bg-light-gray" isMarkdown="false">}}
{{< grid/div class="container research-page-section" >}}

# Consortium
* National Technical University of Athens - Greece
* Consorzio Nazionale Interuniversitario per le Telecomunicazioni - Italy
* SIEMENS AKTIENGESELLSCHAFT - Germany
* ATOS IT - Spain
* Institut National de Recherche en Informatique et Automatique - France
* University of Macedonia - Greece
* Fundingbox Accelerator Sp. Z o.o. - Polland
* Odin Solutions SL - Spain
* ZURCHER HOCHSCHULE FUR ANGEWANDTE WISSENSCHAFTEN - Swisszerland
* Alter Way - France
* Internet Institute Ltd - Slovenia
* ECLIPSE FOUNDATION EUROPE GMBH - Germany
* WINGS ICT SOLUTIONS INFORMATION & COMMUNICATION TECHNOLOGIES IKE - Greece
* IBM IRELAND LIMITED - Ireland
* ESAOTE SPA - Italy
* LUKA KOPER, PORT AND LOGISTIC SYSTEM, D.D. - Slovenia
* W3C/ERCIM - France

{{</ grid/div>}}
{{</ grid/div>}}
