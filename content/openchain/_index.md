---
title: OpenChain
headline: OpenChain Conformance
tagline: All Eclipse Foundation project releases are fully OpenChain Conformant
date: 2022-06-29
description: "All Eclipse Foundation project releases are fully OpenChain Conformant"
hide_sidebar: true
hide_page_title: true
header_wrapper_class: "header-no-split header-rings-bg-img"
container: "container-wrapper-openchain"
page_css_file: "/public/css/projects-openchain.css"
---

<!-- Top 2 columns -->
{{< grid/div class="background-light-gray" isMarkdown="false">}}
  <div class="container">
    <div class="row">
      <div class="col-sm-7 background-dark-brown description-bg">
        <div class="description-bg-content">
          <div class="description-img-wrapper">
            <img src="images/openchain-color.svg"/>
          </div>

          <div class="description-text-wrapper">
            <p>
              All projects at the Eclipse Foundation are developed under a process that conforms to the ISO 5230 standard,
              making them full OpenChain conformant.
            </p>

            <br />
            <br />

            <p>
              The OpenChain ISO 5230 standard is officially known as the OpenChain 2.1 ISO/IEC 5230:2020 standard and is based
              on the <a href="https://www.openchainproject.org/">OpenChain Project</a>. The goal of this project is to provide a
              clear and effective standard for organizations of all sizes, in all industries, and in all markets to
              benefit from a more efficient and effective open source supply chain.
            </p>
          </div>
        </div>
      </div>
      <div class="col-sm-17">
        <div class="what-makes-us-content">
          <h2>What Makes Us Conformant</h2>

          <div class="row what-makes-us-row">
            <div class="col-sm-12">
              <div class="row">
                <div class="col-sm-6">
                  <div class="icon-blue-label">
                    <img class="img-responsive" src="images/icons/copyright.png"/>
                  </div>
                </div>
                <div class="col-sm-18 what-makes-us-text">
                  <p>Our well-defined <a href="/org/documents/Eclipse_IP_Policy.pdf">Intellectual Property Policy</a>&semi;</p>
                </div>
              </div>
            </div>
            <div class="col-sm-12">
              <div class="row">
                <div class="col-sm-6">
                  <div class="icon-blue-label">
                    <img class="img-responsive" src="images/icons/intellectual-property.png"/>
                  </div>
                </div>
                <div class="col-sm-18 what-makes-us-text">
                  <p>Our well-defined intellectual property <a href="/projects/handbook/#ip">due diligence process</a>&semi;</p>
                </div>
              </div>
            </div>
          </div>

          <div class="row what-makes-us-row">
            <div class="col-sm-12">
              <div class="row">
                <div class="col-sm-6">
                  <div class="icon-blue-label">
                    <img class="img-responsive" src="images/icons/intellectual-property-2.png"/>
                  </div>
                </div>
                <div class="col-sm-18 what-makes-us-text">
                  <p>Extensive documentation and comprehensive training materials for all participants in the intellectual property due diligence process;</p>
                </div>
              </div>
            </div>
            <div class="col-sm-12">
              <div class="row">
                <div class="col-sm-6">
                  <div class="icon-blue-label">
                    <img class="img-responsive" src="images/icons/communication-center.png"/>
                  </div>
                </div>
                <div class="col-sm-18 what-makes-us-text">
                  <p>Our full-time professional intellectual property team managing the process&semi;</p>
                </div>
              </div>
            </div>
          </div>

          <div class="row what-makes-us-row">
            <div class="col-sm-12">
              <div class="row">
                <div class="col-sm-6">
                  <div class="icon-blue-label">
                    <img class="img-responsive" src="images/icons/intellectual-property-3.png"/>
                  </div>
                </div>
                <div class="col-sm-18 what-makes-us-text">
                  <p>Governance processes that ensure conformance documentation is in place.</p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
{{</ grid/div >}}

<!-- Project Adopters -->
{{< grid/div class="container" >}}

## OpenChain Conformance for Eclipse Foundation Project Adopters

OpenChain Conformance is all about building trust in the open source software throughout the entire supply chain. Knowing that all Eclipse Foundation projects are OpenChain Conformant helps open source become predictable, understandable and optimized for internal and external supply chains of any type.

ISO5230 conformance helps businesses:

* Easily adopt open source and commercial products based on the conforming components
* Meet ISO5230 requirements in commercial RFQs
* Learn from our conformance processes as they implement their own ISO5230 conformance processes
* Get to market faster by using the Eclipse Foundation’s technologies developed under established and mature processes

OpenChain conformance is just one more way the Eclipse Foundation is helping drive innovation with commercially viable open source software.

***

Learn More and Get Involved

* Review the [Eclipse Foundation Development Process](/projects/dev_process/)
* Get the [ISO/IEC 5230:2020 OpenChain Specification](https://www.iso.org/standard/81039.html)
* Read more about the [OpenChain Project and community](https://www.openchainproject.org/)

{{</ grid/div >}}
