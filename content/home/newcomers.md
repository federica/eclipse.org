---
title: Eclipse Newcomers FAQ
date: 2022-01-06T19:22:17.000Z
description: ""
categories: []
keywords: []
slug: ""
toc: false
draft: false
lastmod: 2022-03-21T18:52:07.137Z
---

## What is Eclipse?

Eclipse is an open source community whose projects are focused on building an extensible development platform, runtimes and application frameworks for building, deploying and managing software across the entire software lifecycle. Many people know us, and hopefully love us, as a Java IDE but Eclipse is much more than a Java IDE.

The Eclipse open source community has over 200 open source projects. These projects can be conceptually organized into seven different "pillars" or categories:

1.  Enterprise Development
2.  Embedded and Device Development
3.  Rich Client Platform
4.  Rich Internet Applications
5.  Application Frameworks
6.  Application Lifecycle Management (ALM)
7.  Service Oriented Architecture (SOA)

The Eclipse community is also supported by a large and vibrant ecosystem of major IT solution providers, innovative start-ups, universities and research institutions and individuals that extend, support and complement the Eclipse Platform.

One very exciting thing about Eclipse is that many people are using Eclipse in ways that we have never imagined. The common thread is that they are building innovative, industrial-strength software and want to use great tools, frameworks and runtimes to make their job easier.

## What is the Eclipse Foundation?

The Eclipse Foundation is a not-for-profit, member-supported corporation that hosts the Eclipse projects. The Foundation provides services to run the IT infrastructure, perform IP due diligence, mentor open source projects, and provide marketing and business development support for the Eclipse community.

It is important to realize that the Eclipse Foundation does not actually develop the open source code. All of the open source software at Eclipse is developed by open source developers, called committers, which are volunteered or contributed by organizations and individuals.

## How can I get started using Eclipse?

Most people start by downloading [one of the different download packages](http://www.eclipse.org/downloads/). Plugins can be added to Eclipse to expand its functionality; the [Eclipse Projects](/projects/listofprojects.php) and [Eclipse Marketplace](http://marketplace.eclipse.org/) are two sources for additional plugins, but there are many more out there!

There is also a wealth of [books](/resources?type=book), [tutorials and white papers](http://www.eclipse.org/resources?category=Getting%20Started) to help you get started. Check out [The Official Eclipse FAQ](http://wiki.eclipse.org/The_Official_Eclipse_FAQs) in the [Eclipse Wiki](http://wiki.eclipse.org/Main_Page) for more information about all aspects of Eclipse. There are also companies available to help with [training and consulting services.](http://marketplace.eclipse.org/taxonomy/term/34) In addition, please feel free to post a question on our [forums.](/forums/)

## Where do I get support?

Everyone is free to open [bugs](http://bugs.eclipse.org/bugs/) and [ask questions on our forums.](/forums/) There are also companies that offer commercial products built on Eclipse and [Eclipse distributions](http://www.eclipse.org/downloads/). Many of these organizations are able to provide commercial -level support.

## How do I get in contact with other Eclipse users?

The best way to get in touch with other Eclipse users is via our [forums](/forums/) and [Bugzilla database.](http://bugs.eclipse.org/bugs/) This is typically where people ask and answer questions about their favourite Eclipse projects. You can also find individuals blogging about Eclipse on [PlanetEclipse.org.](http://www.planeteclipse.org)

If you would like to contact the Eclipse Foundation see our [Contact Us section.](/org/foundation/contact.php)

## What license does Eclipse use?

Eclipse uses the Eclipse Public License (EPL). The EPL is a commercially friendly license that allows organizations to include Eclipse software in their commercial products, while at the same time asking those who create derivative works of EPL code to contribute back to the community.

The commercial-friendly nature of the EPL had been proven over and over as hundreds of companies ship Eclipse based products. More information about the EPL can be found in our [Licensing FAQ.](/legal/eplfaq.php)

## How do I start an Eclipse open source project?

The first step in starting an open source project is to familiarize yourself with the [existing open source projects.](/projects) There is a lot of diverse technology being developed at Eclipse, so it might be actually easier to contribute to an existing open source project. If you believe you have a unique technology that is suited for Eclipse, then the first step is to read the Eclipse development process to ensure that the goals of Eclipse align with your interests in creating an open source project. The next step is to contact the Eclipse Management Organization (EMO) to get some help and advice in drafting your proposal. You can contact the EMO via [emo at eclipse dot org](mailto:emo@eclipse.org).

## Who is allowed to start an Eclipse open source project?

Eclipse is an open and transparent community of open source projects. We welcome project proposals from all sources. It is important to understand that you do not have to be a member of the Eclipse Foundation to [start](http://wiki.eclipse.org/Development_Resources/HOWTO/Starting_A_New_Project) or work on an open source project.

## How do I become a member?

To become a member please visit [here](/membership/become_a_member/) for detailed instructions.
